import json
import io
import lib.pyvertex as pyvertex

class Pystudent(pyvertex.Pyvertex):

    KEY_JSON = "students"

    def __init__(self, data, id):
        if isinstance(data,dict):
            super().__init__(data,Pystudent.KEY_JSON,id)
            self.potential_schools = []
            self.school = None

    @property
    def name(self):
        return self.data["name"]

    @property
    def id(self):
        return self.data["id"]

    @property
    def choices(self):
        return self.data["choices"]

    def find_school(self, id):
        return pyvertex.Pyvertex.find_id(self.potential_schools, id)
    
    def __repr__(self):
        return f"({self.name}, {self.choices})"

    def reset_potential_schools(self):
        # create dynamic attribute
        self.potential_schools = []
        self.school = None


    