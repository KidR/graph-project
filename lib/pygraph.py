import json
import io

from lib.pystudent import *
from lib.pyschool import *


class Pygraph:
    """ IMPLEMENTATION OF OUR GRAPH STRUCTURE """

    class NotFoundError(Exception):
        pass

    def __init__(self, data):
        """
            Initialize the graph.

            Args:
                data (str): JSON file's path
                data (io.TextIOWrapper): JSON file input
                data (dict): Graph's data
        """
        if isinstance(data, str):
            # IF path to json file THEN
            with open(data, "r") as json_file:
                self.__data = json.load(json_file)
        elif isinstance(data, io.TextIOWrapper):
            # IF data is a json file input THEN
            self.__data = json.load(data)
        elif isinstance(data, dict):
            # IF data is a dict type THEN
            self.__data = data
        else:
            raise TypeError("***** Data type is not valid.")
        # Instanciations of student and school
        self.__students = {entry["id"]: Pystudent(
            data=self.__data, id=entry["id"]) for entry in self.__data[Pystudent.KEY_JSON]}
        self.__schools = {entry["id"]: Pyschool(
            data=self.__data, id=entry["id"]) for entry in self.__data[Pyschool.KEY_JSON]}
        self.__applicants = []

    @property
    def data(self):
        return self.__data

    @property
    def schools(self):
        return self.__schools

    @property
    def students(self):
        return self.__students

    @students.setter 
    def students(self, students):
        self.__students = students

    @property
    def applicants(self):
        return self.__applicants

    @applicants.setter
    def applicants(self, applicants):
        self.__applicants = applicants

    def __repr__(self):
        res = ""
        for s in self.schools.values():
            res += '- {}:\n'.format(s.name)
            for a in s.applicants:
                res += "  + {}\n".format(a.name)
        return res

    def school_by_id(self, id):
        return self.schools[id]

    def student_by_id(self, id):
        return self.students[id]

    def reset_applicants(self):
        for school in self.schools.values():
            school.reset_applicants()

    def reset_potential_schools(self):
        for student in self.students.values():
            student.reset_potential_schools()
    
    def nb_steps(self):
        return self.nb_steps

    def results(self):
        """ Returns the results of the algorithm in a dictionary

        Format is:
        {
            "students": [
                {
                    "name": <student_name: str>,
                    "school": <school_name: str>
                }, ...
            ],
            "schools": [
                {
                    "name": <school_name: str>,
                    "students": [<student_name: str>, ...]
                }, ...
            ]
        }
        """
        return {
            "students": [{'name': student.name, 'school': student.school.name if student.school else '-'} for student in self.__students.values()],

            "schools": [{'name': school.name, 'students': [s.name for s in school.applicants]} for school in self.__schools.values()]
        }
