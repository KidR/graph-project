from lib.pygraph import Pygraph

class Marriage:
    """Class with Gale-Shapley algorithm apply so schools and students."""

    @classmethod
    def __matching_over(cls, graph: Pygraph, logfile):
        for s in graph.schools.values():
            print('School {}: {} applicants (max: {})'.format(
                s.name, len(s.applicants), s.capacity), file=logfile)
            if len(s.applicants) > s.capacity:
                return False
        return True

    @classmethod
    def __display_state(cls, graph: Pygraph, logfile):
        print('- STUDENTS',file=logfile)
        for student in graph.students.values():
            for school in student.potential_schools:
                print('  +{} -> {}'.format(student.name, school.name),file=logfile)
        print('\n- SCHOOLS',file=logfile)
        for school in graph.schools.values():
            print('  + {} ({}/{})'.format(school.name,
                                            len(school.applicants), school.capacity),file=logfile)
            for a in school.applicants:
                print('    * {}'.format(a.name),file=logfile)

    @classmethod
    def schools_accept_students(cls,graph: Pygraph):
        is_over = False
        step = 0
        excluded_students = []
        logfile = open("main.log","w+")
        while not is_over:
            step += 1
            print('\n+----- STEP {} -----+\n'.format(step),file=logfile)

            graph.reset_applicants()

            # Distribute students in schools
            new_students = []
            for student in graph.students.values():
                if len(student.choices) > 0:
                    school = graph.school_by_id(student.choices[0])
                    print('    +{} go to {} school'.format(student.name, school.name),file=logfile)
                    school.applicants.append(student)  # add applicant
                    new_students.append(student)
                else:
                    excluded_students.append(student)

            graph.students = {student.id: student for student in new_students}

            # List applicants
            for school in graph.schools.values():
                print('\n- {} applicants:'.format(school.name),file=logfile)
                # Accept students
                nb_accepted = 0
                tmp_applicants = []
                for a in school.applicants:
                    print('    +{}'.format(a.name),file=logfile)
                    tmp_applicants.append(a.id)
                
                for pref in school.preferences:
                    if pref in tmp_applicants:
                        a = school.find_applicant(id=pref)
                        if nb_accepted >= school.capacity:
                            # Applicant is refused
                            a.choices.pop(0)
                            print(
                                'x {} is refused in {}'.format(a.name, school.name),file=logfile)
                            a.school = None
                        else:
                            # Applicant is accepted
                            nb_accepted += 1
                            print(
                                '> {} is accepted in {}'.format(a.name, school.name),file=logfile)
                            a.school = school
            
            is_over = Marriage.__matching_over(graph,logfile)

        print('\n% Matching over.',file=logfile)
        print('Algorithm completed in {} steps'.format(step))
        graph.nb_steps = step

        if len(excluded_students) > 0:
            print('{} students are excluded:'.format(len(excluded_students)),file=logfile)
            for excluded_student in excluded_students:
                print(' * {}'.format(excluded_student.name),file=logfile)

            graph.students.update({student.id: student for student in excluded_students})
        logfile.close()
            
    @classmethod
    def students_accept_schools(cls, graph: Pygraph):
        is_over = False
        step = 0
        logfile = open("results/main.log","w+")
        while not is_over:
            step += 1
            print('\n+----- STEP {} -----+\n'.format(step),file=logfile)

            graph.reset_applicants()
            graph.reset_potential_schools()

            Marriage.__display_state(graph,logfile)
            
            # Each school go to see the capacity first students on their list
            for school in graph.schools.values():
                for pref in school.preferences[:school.capacity]:
                    student = graph.student_by_id(pref)
                    school.applicants.append(student)
                    student.potential_schools.append(school)

            
            # Students choose the best school
            for student in graph.students.values():
                applicants_ids = []
                print("  + {}".format(student.name),file=logfile)
                for a in student.potential_schools:
                    applicants_ids.append(a.id)
                for pref in student.choices:
                    if pref in applicants_ids:
                        a: Pyschool = student.find_school(id=pref)
                        if student.school:
                            # School is refused
                            a.preferences.remove(student.id)
                            a.applicants.remove(student)
                            print('    x Refuse {}'.format(a.name),file=logfile)
                        else:
                            # School is accepted
                            student.school = a
                            print('    > Accept {}'.format(
                                a.name, student.name),file=logfile)

            
            # Check matching is over
            is_over = True
            for student in graph.students.values():
                print('- {}: {} applicants'.format(student.name,
                                                   len(student.potential_schools)),file=logfile)
                if len(student.potential_schools) > 1:
                    is_over = False
            Marriage.__display_state(graph,logfile)

        # Over
        print('\n% Matching over.',file=logfile)
        print('Algorithm completed in {} steps'.format(step))
        graph.nb_steps = step
        logfile.close()