import json
import io
import lib.pyvertex as pyvertex

class Pyschool(pyvertex.Pyvertex):

    KEY_JSON = "schools"

    def __init__(self, data, id):
        if isinstance(data,dict):
            super().__init__(data,Pyschool.KEY_JSON,id)
            self.__applicants = []

    @property
    def name(self):
        return self.data["name"]

    @property
    def id(self):
        return self.data["id"]

    @property
    def capacity(self):
        return self.data["capacity"]

    @property
    def preferences(self):
        return self.data["preferences"]

    @property
    def applicants(self):
        return self.__applicants

    @applicants.setter
    def applicants(self, applicants):
        self.__applicants = applicants

    def find_applicant(self,id):
        return pyvertex.Pyvertex.find_id(self.applicants,id)
    
    def __repr__(self):
        return f"({self.name},{self.preferences})"

    def reset_applicants(self):
        self.__applicants = []

    