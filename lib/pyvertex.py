import json
import io

class Pyvertex:

    def __init__(self, data, key, id):
        if isinstance(data,io.TextIOWrapper):
            self.__data = Pyvertex.find_id(json.load(data)[key],id)
        elif isinstance(data,dict):
            self.__data = Pyvertex.find_id(data[key],id)

    @staticmethod
    def find_id(entry, id):
        for vertex in entry:
            if isinstance(vertex, dict):
                if vertex['id'] == id:
                    return vertex
            else:
                if vertex.id == id:
                    return vertex
    
    @property
    def data(self):
        return self.__data
