
import json
from pygraph import Pygraph

class Generator:
    """ Abstract class that is used to generate the result for the graph """

    DIR_RESULT = "results"

    def generate(self, graph: Pygraph):
        """ Generates the result """
        pass

class JsonGenerator(Generator):
    def __init__(self, path="result"):
        self.path = f"{Generator.DIR_RESULT}/{path}"

    def generate(self, graph: Pygraph):
        with open(f"{self.path}.json", 'w+') as fout:
            json.dump(graph.results(), fout, indent=4)

class CsvGenerator(Generator):
    def __init__(self, path="result"):
        self.path = f"{Generator.DIR_RESULT}/{path}"
    
    def generate(self, graph: Pygraph):
        with open(f"{self.path}.csv", 'w+') as fout:
            # Header
            fout.write('STUDENT, SCHOOL\n')
            # Data
            for student in graph.results()['students']:
                fout.write('{}, {}\n'.format(student['name'], student['school']))

class GraphGenerator(Generator):

    def __init__(self, path="result"):
        self.path = f"{Generator.DIR_RESULT}/{path}"

    def generate(self, graph: Pygraph):
        str_data = ""
        with open(".html_template","r") as template:
            str_template = template.read()

            for student in graph.students.values():
                str_data += json.dumps( {"data": {"id": student.name, "group": "nodes"}} )+","
                
            for school in graph.schools.values():
                str_data += json.dumps( {"data": {"id": school.name, "group": "nodes"}} )+","

            i=0
            for student in graph.results()['students']:
                if student["school"] != "-":
                    str_data += json.dumps( {"data": {"id": i, "source": student["name"], "target": student["school"], "group": "edges"}} )+","
                    i+=1

            str_template = str_template.replace("@data@",str_data[:-1])

        with open(f"{self.path}.html","w+") as result:
            result.write(str_template)

