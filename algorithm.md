# Pseudo-code for the algorithm

```
While not each school has at most n applicants loop
    For each student loop
        If the student as a school on his list then
            The student go to this scool
        Else
            The student is removed
        End if
    End loop
    For each school loop
        Keep the n top applicant students
    End loop
End loop
```

```
While not each student has at most 1 school loop
    For each school loop
        If the school as a student on its list then
            The school stays with the student
        Else
            The school is removed
        End if
        For each student loop
            Keep the best school
        End loop
    End loop
End loop
```