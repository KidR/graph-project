# Subject : Stable Marriage Problem

## Introduction

To solve the **Stable Marriage** problem, we used the *Gale-Shapley algorithm* seen in class. You can find the specification [here](https://docs.google.com/document/d/1F0V6ztA-BnojlsDfhCB0tyQg3Kk39q472KN1czaLVwc/edit?usp=sharing). We have chosen Python3 for the implementation of the algorithm.

### Conception

We used the concepts of oriented-object programming. You'll find all classes in `lib` :

- `Pygraph` : implementation of the graph
- `Pyvertex` : abstract implementation of vertices which is used by `Pygraph`
- `Pystudent` : class representing vertex as a student
- `Pyschool` : class representing vertex as a school

### Run

To run our program, simply enter `python3 main.py` at the root of the project.

## Graph format

We decided to implement our graph in this way

```json
{
    "students" :
        [ 
            {
                "name" : "a",           // Student's name
                "id" : 1,               // Student's ID
                "choices" : [1,2,3]     // School IDs in order of preference
            },
            { ... }
        ],
    "schools" :
        [
            {
                "name" : "A",           // School name
                "id" : 1,               // School ID
                "capacity" : 120,       // Number of students that can be accepted
                "preferences" : [1,3,2] // Student IDs in order of preference
            },
            { ... }
        ]
}
```

Our program only require 1 input file in JSON format. By default, we provided simple test cases and more complex files to test the program. But you can use your own input if you need to. Just make sure to respect the specification of the input file.

## Result format

### JSON format

```json
{
    "students": [
        {
            "name": "Amelia",
            "school": "Aviation Civile"
        }
    ],
  	"schools": [
        {
            "name": "ENSEEIHT",
            "students": [
                "Edward",
                "Rhys"
            ]
        }
    ]
}
```

### CSV format

```csv
STUDENT,SCHOOL
Amelia,Aviation Civile
Corey,-
Kayleigh,Purpan
```

Where `STUDENT` and `SCHOOL` are headers.
When an student couldn't find a school, the `-` character is printed.

### Graph visualisation

The graph visualisation is based on a html template ([`.html_template`](.html_template)) which uses a javascript framework called [`cystoscape.js`](https://js.cytoscape.org/).

Even if the visualisation of the result was not required to be graphical, we thought it would be more interesting to see it that way. Plus, since its made in javascript, it doesn't require any additional python dependency.

In the case where several students have the same name, the algorithm will work fine but the visualization of the graph is unable to distinguish between these students. Then these students will be represented by the same vertex in the graph.

## Test cases
JSON files for each test case can be found under `data/test_case_X.json`.

1. More students than schools slots
1. More schools slots than students
1. Same number of schools slots, same number of students
1. Schools without any student
1. More students than schools
1. More schools than students

Code for executing tests is located at the root of the project, in the `test.py` file.
