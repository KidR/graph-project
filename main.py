import json
import sys
import os

sys.path.append("./lib")

from marriage import Marriage
from pygraph import *
from generator import *


INSTRUCTIONS = """
Welcome to the lightweight student-schools matching platform.
Press <Ctrl+C> to leave at any time.
Use <Number Key> to use the program.
By Ly R. and Quema T.
"""

INPUT_SYMBOLE = "$> "

def main():
    print(INSTRUCTIONS)
    print("""Which algorithm do you want to apply ?
    1- Students accepts schools
    2- Schools aceepts students""")
    algorithm = Marriage.students_accept_schools if int(input(INPUT_SYMBOLE)) == 1 else Marriage.schools_accept_students

    print("""Which dataset do you want to use ?
    """)
    dataset = os.listdir("./data")
    no_choice = True
    while no_choice:
        for cpt in range(len(dataset)):
            print(f"\t{cpt+1}- {dataset[cpt]}")
        choice = int(input(INPUT_SYMBOLE))
        no_choice = False if choice > 0 and choice <= len(dataset) else True
    
    graph = Pygraph(data=f"./data/{dataset[choice-1]}")

    # a enlever a la version finale ?
    print("Loaded {} students and {} schools!".format(len(graph.students), len(graph.schools)))
    print('Students:')
    for s in graph.students.values():
        print(' * {}'.format(s.name))
    print('Schools:')
    for s in graph.schools.values():
        print(' * {} ({})'.format(s.name, s.capacity))
    
    algorithm(graph) # applying the algorithm
    
    result_name = input(f"{INPUT_SYMBOLE}Result's filename: ")
    generators = [JsonGenerator(result_name), CsvGenerator(result_name), GraphGenerator(result_name)]

    no_choice = True
    while no_choice:
        print("""Which type of result do you want ?
    1- JSON file
    2- CSV file
    3- Graph visualization file
    4- All type of file""")
        choice = int(input(INPUT_SYMBOLE))
        no_choice = False if choice > 0 and choice <= len(generators)+1 else True
    
    if choice == 4:
        for gen in generators:
            gen.generate(graph)
    else:
        generators[choice-1].generate(graph)
    print("Thank you for using our program.")

if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("\n*****Thank you for using our program.")
    
    
