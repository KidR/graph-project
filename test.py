import unittest
import sys
from lib.pygraph import Pygraph
from lib.marriage import Marriage

class TestSchoolsAcceptStudents(unittest.TestCase):
    def test_1_more_students_than_school_slots(self):
        graph = Pygraph('data/test_case_1.json')
        Marriage.schools_accept_students(graph)
        expected_results = [
            {
                "name": "Amelia",
                "school": "UT3"
            },
            {
                "name": "Corey",
                "school": "ENSEEIHT"
            },
            {
                "name": "Kayleigh",
                "school": "-"
            },
            {
                "name": "Luca",
                "school": "ENSEEIHT"
            },
            {
                "name": "Jay",
                "school": "UT3"
            }
        ]
        for student in graph.results()['students']:
            print("Student: " + student['name'])
            self.assertTrue(student in expected_results)

    def test_2_more_school_slots_than_students(self):
        graph = Pygraph('data/test_case_2.json')
        Marriage.schools_accept_students(graph)
        expected_results = {
            "Amelia": "ENSEEIHT",
            "Corey": "UT3",
            "Kayleigh": "ENSEEIHT",
            "Luca": "UT3",
            "Jay": "ENSEEIHT"
        }

        for student in graph.results()['students']:
            self.assertEqual(expected_results[student['name']], student['school'])
    
    def test_3_same_number_of_school_slots_same_number_of_students(self):
        graph = Pygraph('data/test_case_3.json')
        Marriage.schools_accept_students(graph)
        expected_results = {
            "Amelia": "UT3",
            "Corey": "UT3",
            "Kayleigh": "ENSEEIHT",
            "Luca": "UT3",
            "Jay": "ENSEEIHT"
        }

        for student in graph.results()['students']:
            self.assertEqual(expected_results[student['name']], student['school'])
        self.assertEqual(graph.nb_steps, 2)

    def test_4_schools_without_any_student(self):
        graph = Pygraph('data/test_case_4.json')
        Marriage.schools_accept_students(graph)
        expected_results = {
            "Amelia": "UT3",
            "Corey": "UT3",
            "Kayleigh": "ENSEEIHT",
            "Luca": "UT3",
            "Jay": "ENSEEIHT"
        }

        for student in graph.results()['students']:
            self.assertEqual(expected_results[student['name']], student['school'])
        self.assertEqual(graph.nb_steps, 2)


class TestStudentsAcceptSchools(unittest.TestCase):
    def test_1_more_students_than_school_slots(self):
        graph = Pygraph('data/test_case_1.json')
        Marriage.students_accept_schools(graph)
        expected_results = {
            "ENSEEIHT": {
                "Luca",
                "Corey"
            },
            "UT3": {
                "Amelia",
                "Jay"
            }
        }

        for school in graph.results()['schools']:
            for student in school['students']:
                self.assertTrue(student in expected_results[school['name']])
        self.assertEqual(graph.nb_steps, 1)

    def test_2_more_school_slots_than_students(self):
        graph = Pygraph('data/test_case_2.json')
        Marriage.students_accept_schools(graph)
        expected_results = {
            "ENSEEIHT": {
                "Kayleigh",
                "Jay",
                "Amelia"
            },
            "UT3": {
                "Luca",
                "Corey"
            }
        }

        for school in graph.results()['schools']:
            for student in school['students']:
                self.assertTrue(student in expected_results[school['name']])
        self.assertEqual(graph.nb_steps, 6)

    def test_3_same_number_of_school_slots_same_number_of_students(self):
        graph = Pygraph('data/test_case_3.json')
        Marriage.students_accept_schools(graph)
        expected_results = {
            "Amelia": "UT3",
            "Corey": "ENSEEIHT",
            "Kayleigh": "UT3",
            "Luca": "ENSEEIHT",
            "Jay": "UT3"
        }

        for student in graph.results()['students']:
            self.assertEqual(expected_results[student['name']], student['school'])
        self.assertEqual(graph.nb_steps, 1)

    def test_4_schools_without_any_student(self):
        graph = Pygraph('data/test_case_4.json')
        Marriage.students_accept_schools(graph)
        expected_results = {
            "Amelia": "UT3",
            "Corey": "ENSEEIHT",
            "Kayleigh": "UT3",
            "Luca": "ENSEEIHT",
            "Jay": "UT3"
        }

        for student in graph.results()['students']:
            self.assertEqual(expected_results[student['name']], student['school'])
        self.assertEqual(graph.nb_steps, 6)
                

        
if __name__ == "__main__":
    unittest.main()